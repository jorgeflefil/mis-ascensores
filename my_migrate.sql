BEGIN;
--
-- Create model Ascensor
--
CREATE TABLE "aplicacion_ascensor" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "modelo_ascensor" varchar(20) NOT NULL);
--
-- Create model Cliente
--
CREATE TABLE "aplicacion_cliente" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "nombre_cliente" varchar(100) NOT NULL, "direccion_cliente" varchar(100) NOT NULL, "ciudad_cliente" varchar(100) NOT NULL, "comuna_cliente" varchar(100) NOT NULL, "telefono_cliente" varchar(100) NOT NULL, "correo_cliente" varchar(100) NOT NULL);
--
-- Create model OrdenTrabajo
--
CREATE TABLE "aplicacion_ordentrabajo" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "fecha_ot" date NOT NULL, "hora_inicio_ot" time NOT NULL, "hora_termino_ot" time NULL, "fallas_ot" varchar(600) NULL, "reparacion_ot" varchar(600) NULL, "cambio_piezas_ot" varchar(400) NULL, "ascensor_ot_id" integer NOT NULL REFERENCES "aplicacion_ascensor" ("id") DEFERRABLE INITIALLY DEFERRED, "cliente_ot_id" integer NOT NULL REFERENCES "aplicacion_cliente" ("id") DEFERRABLE INITIALLY DEFERRED, "usuario_ot_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED);
CREATE INDEX "aplicacion_ordentrabajo_ascensor_ot_id_9649e07e" ON "aplicacion_ordentrabajo" ("ascensor_ot_id");
CREATE INDEX "aplicacion_ordentrabajo_cliente_ot_id_4ccc3b47" ON "aplicacion_ordentrabajo" ("cliente_ot_id");
CREATE INDEX "aplicacion_ordentrabajo_usuario_ot_id_2e44bed0" ON "aplicacion_ordentrabajo" ("usuario_ot_id");
COMMIT;

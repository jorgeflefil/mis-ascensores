# Importar Sesiones
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash

# Importar Decorators
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import user_passes_test

# Importar Redireccionamientos
from django.http import HttpResponse
from django.shortcuts import redirect, render

# Importar Usuarios y grupos
from django.contrib.auth.models import User, Group

# Importar Clases
from .models import Cliente
from .models import OrdenTrabajo
from .models import Ascensor


# Rest Framework
from rest_framework import viewsets
from .serializer import UserSerializer
from .serializer import ClienteSerializer


# Create your views here.

@login_required(login_url='login')
def index(request):
    return render(request, 'index.html', {'lista_ot': OrdenTrabajo.objects.all()})


def registro_tecnico(request):
    # User
    registro_nombre = request.POST.get('registro_nombre', '')
    registro_apellido = request.POST.get('registro_apellido', '')
    registro_email = request.POST.get('registro_email', '')
    registro_contrasenia = request.POST.get('registro_contrasenia', '')

    user = User.objects.create_user(username=registro_email, first_name=registro_nombre, last_name=registro_apellido, email=registro_email,
                                    password=registro_contrasenia)
    user.save()
    return redirect('index')


def login(request):
    return render(request, 'login.html', {})


def logearse(request):
    nombre = request.POST.get("usuario", '')
    contrasenia = request.POST.get("contrasenia", '')

    user = authenticate(request, username=nombre, password=contrasenia)

    print(nombre, contrasenia)
    print(user)

    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.username
        return redirect("index")
    else:
        messages.error(request, 'El usuario o la contraseña no es válido ')
        return redirect("login")


@login_required(login_url='login')
def cerrar_sesion(request):
    logout(request)
    return redirect("login")


def administrador_clientes(request):
    return render(request, 'administrador_clientes.html', {'lista_clientes': Cliente.objects.all()})


def cliente_crear(request):
    nombre_cliente = request.POST.get('nombre_cliente', '')
    direccion_cliente = request.POST.get('direccion_cliente', '')
    ciudad_cliente = request.POST.get('ciudad_cliente', '')
    comuna_cliente = request.POST.get('comuna_cliente', '')
    telefono_cliente = request.POST.get('telefono_cliente', '')
    correo_cliente = request.POST.get('correo_cliente', '')

    cliente = Cliente(nombre_cliente=nombre_cliente, direccion_cliente=direccion_cliente, ciudad_cliente=ciudad_cliente,
                      comuna_cliente=comuna_cliente, telefono_cliente=telefono_cliente, correo_cliente=correo_cliente)
    cliente.save()
    return redirect('administrador_clientes')



def registro_tecnico(request):
    # User
    registro_nombre = request.POST.get('registro_nombre', '')
    registro_apellido = request.POST.get('registro_apellido', '')
    registro_email = request.POST.get('registro_email', '')
    registro_contrasenia = request.POST.get('registro_contrasenia', '')

    user = User.objects.create_user(username=registro_email, first_name=registro_nombre, last_name=registro_apellido, email=registro_email,
                                    password=registro_contrasenia)
    user.save()
    return redirect('index')


def eliminar(request, id):
    cliente = Cliente.objects.get(pk=id)
    cliente.delete()
    return redirect('administrador_clientes')


def administrador_tecnicos(request):
    return render(request, 'administrador_tecnicos.html', {'lista_clientes': Cliente.objects.all(), 'lista_usuarios': User.objects.all()})


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class ClienteViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer


def orden_trabajo_crear(request):
    #Recuperar Datos
    tecnico_seleccionado = request.POST.get('tecnico_seleccionado','')
    usuario = User.objects.get(pk=tecnico_seleccionado)

    cliente_seleccionado = request.POST.get('cliente_seleccionado','')
    cliente = Cliente.objects.get(pk=cliente_seleccionado)

    ascensor_seleccionado = request.POST.get('ascensor_seleccionado','')
    ascensor = Ascensor(modelo_ascensor=ascensor_seleccionado)
    ascensor.save()

    #Asignar Datos
    usuario_ot = usuario
    cliente_ot = cliente
    ascensor_ot = ascensor

    fecha_ot = request.POST.get('fecha_ot','1918-12-01')
    hora_inicio_ot = request.POST.get('hora_inicio_ot','')
    """
    hora_termino_ot = request.POST.get('hora_termino_ot','')
    fallas_ot = request.POST.get('fallas_ot','')
    reparacion_ot = request.POST.get('reparacion_ot','')
    cambio_piezas_ot = request.POST.get('cambio_piezas_ot','')
    """
    ot = OrdenTrabajo(usuario_ot=usuario_ot, cliente_ot=cliente_ot, ascensor_ot=ascensor_ot, fecha_ot=fecha_ot,
                        hora_inicio_ot=hora_inicio_ot, hora_termino_ot=None, fallas_ot=None,
                        reparacion_ot=None, cambio_piezas_ot=None)
    ot.save()
    return redirect('index')


    

from django.urls import path
from rest_framework import routers
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'clientes', views.ClienteViewSet)

urlpatterns = [
    path('login/registro', views.registro_tecnico, name="registro_tecnico"),
    path('login', views.login, name="login"),
    path('login/logearse', views.logearse, name="logearse"),
    path('login/auth/', include('social_django.urls', namespace='social')),

    path('', views.index, name="index"),
    path('cerrar_sesion', views.cerrar_sesion, name="cerrar_sesion"),
    path('administrador/clientes', views.administrador_clientes,
         name="administrador_clientes"),
    path('administrador/clientes/crear',
         views.cliente_crear, name="cliente_crear"),
    path('administrador/clientes/eliminar/<int:id>',
         views.eliminar, name="eliminar"),

    path('administrador/tecnicos', views.administrador_tecnicos,name="administrador_tecnicos"),
    path('administrador/tecnicos/crear',views.orden_trabajo_crear, name="orden_trabajo_crear"),

    path('api/', include(router.urls)),

    path('sw.js', (TemplateView.as_view(template_name="sw.js", content_type='application/javascript', )), name='sw.js'),
    path('manifest.json', (TemplateView.as_view(template_name="manifest.json", content_type='application/json', )), name='manifest.json'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Cliente


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'first_name', 'last_name')


class ClienteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Cliente
        fields = ('url', 'nombre_cliente', 'direccion_cliente', 'ciudad_cliente',
                  'comuna_cliente', 'telefono_cliente', 'correo_cliente')

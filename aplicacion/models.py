from django.db import models
from django import forms
from django.contrib.auth.models import User, Group
from rest_framework import serializers

# Signals para Perfiles
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.


class Cliente(models.Model):
    nombre_cliente = models.CharField(max_length=100)
    direccion_cliente = models.CharField(max_length=100)
    ciudad_cliente = models.CharField(max_length=100)
    comuna_cliente = models.CharField(max_length=100)
    telefono_cliente = models.CharField(max_length=100)
    correo_cliente = models.CharField(max_length=100)

    def __str__(self):
        return "Cliente"


class Ascensor(models.Model):
    modelo_ascensor =models.CharField(max_length=20)

    def __str__(self):
        return "Ascensor"

"""
class User(models.Model):
    registro_nombre = models.CharField(max_length=100)
    registro_apellido = models.CharField(max_length=100)
    registro_email = models.CharField(max_length=100)
    registro_contrasenia = models.CharField(max_length=8)
    
    def __str__(self):
        return "User"
"""

class OrdenTrabajo(models.Model):
    usuario_ot = models.ForeignKey(User, on_delete=models.CASCADE)
    cliente_ot = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    ascensor_ot = models.ForeignKey(Ascensor, on_delete=models.CASCADE)

    fecha_ot = models.DateField(auto_now_add=True, blank=True)
    hora_inicio_ot = models.TimeField(auto_now_add=True, blank=True)
    hora_termino_ot = models.TimeField(null=True)
    fallas_ot = models.CharField(null=True, max_length=600)
    reparacion_ot = models.CharField(null=True, max_length=600)
    cambio_piezas_ot = models.CharField(null=True,max_length=400)

    def __str__(self):
        return "OrdenTrabajo"
